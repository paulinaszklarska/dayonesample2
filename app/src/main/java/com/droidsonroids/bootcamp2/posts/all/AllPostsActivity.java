package com.droidsonroids.bootcamp2.posts.all;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.droidsonroids.bootcamp2.Constants;
import com.droidsonroids.bootcamp2.R;
import com.droidsonroids.bootcamp2.model.Post;
import com.droidsonroids.bootcamp2.posts.BasePostActivity;
import com.droidsonroids.bootcamp2.posts.add.NewPostActivity;
import com.droidsonroids.bootcamp2.posts.edit.EditPostActivity;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AllPostsActivity extends BasePostActivity {

	private SwipeRefreshLayout mSwipeRefreshLayout;
	private ListView mListView;
	private SwipeRefreshLayout mEmptyPostsSwipeRefreshLayout;
	private PostsAdapter mPostsAdapter;
	private FloatingActionButton mFloatingActionButton;
	private CoordinatorLayout mCoordinatorLayout;

	@Override protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_all_posts);
		findViews();
		initializeListView();
		initializeSwipeRefreshLayouts();
		setOnClickListeners();
	}

	private void setOnClickListeners() {
		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
				handleOnItemClick(position);
			}
		});
		mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override public boolean onItemLongClick(final AdapterView<?> parent, final View view, final int position, final long id) {
				handleOnItemLongClick(position);
				return true;
			}
		});
		mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(final View v) {
				handleOnFabClick();
			}
		});
	}

	private void handleOnFabClick() {
		Intent newPostActivityIntent = new Intent(AllPostsActivity.this, NewPostActivity.class);
		startActivityForResult(newPostActivityIntent, Constants.REQUEST_CODE_NEW_POST);
	}

	private void handleOnItemLongClick(final int position) {
		new AlertDialog.Builder(AllPostsActivity.this)
				.setMessage(R.string.delete_confirmation)
				.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

					@Override public void onClick(DialogInterface dialog, int which) {
						deletePost(position);
					}
				})
				.setNegativeButton(R.string.no, null)
				.show();
	}

	private void deletePost(final int position) {
		final Post post = mPostsAdapter.getItem(position);
		new AsyncTask<Post, Void, Boolean>() {

			@Override protected Boolean doInBackground(final Post... params) {
				Post post = params[0];
				Request deleteRequest = buildDeleteRequest(post);
				try {
					Response response = mHttpClient.newCall(deleteRequest).execute();
					return response.isSuccessful();
				} catch (IOException e) {
					return false;
				}
			}

			@Override protected void onPostExecute(final Boolean success) {
				if (success) {
					mPostsAdapter.deleteItem(post);
					Snackbar.make(mCoordinatorLayout, R.string.success_delete_post, Snackbar.LENGTH_SHORT).show();
				} else {
					Snackbar.make(mCoordinatorLayout, R.string.error_request_delete, Snackbar.LENGTH_SHORT).show();
				}
			}
		}.execute(post);
	}

	private Request buildDeleteRequest(final Post post) {
		String url = buildDeleteRequestUrl(post.getObjectId());
		return getRequestBuilderWithHeaders().url(url).delete().build();
	}

	private void handleOnItemClick(final int position) {
		Post post = mPostsAdapter.getItem(position);
		Intent editPostIntent = new Intent(AllPostsActivity.this, EditPostActivity.class);
		editPostIntent.putExtra(Constants.EXTRA_POST, post);
		startActivityForResult(editPostIntent, Constants.REQUEST_CODE_EDIT_POST);
	}

	private String buildDeleteRequestUrl(final String objectId) {
		return POSTS_URL + "/" + objectId;
	}

	private void initializeSwipeRefreshLayouts() {
		SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
			@Override public void onRefresh() {
				refreshPosts();
			}
		};
		mSwipeRefreshLayout.setOnRefreshListener(onRefreshListener);
		mEmptyPostsSwipeRefreshLayout.setOnRefreshListener(onRefreshListener);
	}

	private void refreshPosts() {
		new AsyncTask<Void, Void, List<Post>>() {

			@Override protected List<Post> doInBackground(final Void... params) {
				Request getAllPostsRequest = buildGetRequest();
				try{
					Response response = mHttpClient.newCall(getAllPostsRequest).execute();
					if (response.isSuccessful()) {
						return parseResponse(response.body().string());
					} else {
						return null;
					}
				} catch (IOException | JSONException e) {
					return null;
				}
			}

			@Override protected void onPostExecute(final List<Post> posts) {
				if (posts != null) {
					mPostsAdapter.setPosts(posts);
				} else {
					Snackbar.make(mCoordinatorLayout, R.string.error_request_get, Snackbar.LENGTH_SHORT).show();
				}
				mSwipeRefreshLayout.setRefreshing(false);
				mEmptyPostsSwipeRefreshLayout.setRefreshing(false);
			}
		}.execute();
	}

	private Request buildGetRequest() {
		String url = buildGetRequestUrl();
		return getRequestBuilderWithHeaders()
				.url(url)
				.get()
				.build();
	}

	private String buildGetRequestUrl() {
		return POSTS_URL;
	}

	private void initializeListView() {
		mListView.setEmptyView(mEmptyPostsSwipeRefreshLayout);
		mPostsAdapter = new PostsAdapter(getLayoutInflater());
		mListView.setAdapter(mPostsAdapter);
	}

	private void findViews() {
		mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.activity_all_posts_coordinator_layout);
		mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_all_posts_swipe_refresh_layout);
		mListView = (ListView) findViewById(R.id.activity_all_posts_listview);
		mEmptyPostsSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_all_posts_empty_view_swipe_refresh_layout);
		mFloatingActionButton = (FloatingActionButton) findViewById(R.id.activity_all_posts_fab);
	}

	private List<Post> parseResponse(final String response) throws JSONException {
		List<Post> postList = new ArrayList<>();
		JSONObject responseJsonObject = new JSONObject(response);
		JSONArray postsJsonArray = responseJsonObject.getJSONArray("results");
		for (int i = 0; i < postsJsonArray.length(); i++) {
			JSONObject jsonObject = postsJsonArray.getJSONObject(i);
			Post newPost = Post.fromJsonObject(jsonObject);
			postList.add(newPost);
		}
		return postList;
	}

	@Override protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		if (resultCode == RESULT_OK) {
			if (requestCode == Constants.REQUEST_CODE_EDIT_POST) {
				refreshPostsWithSnackbar(R.string.success_edit_post);
			} else if (requestCode == Constants.REQUEST_CODE_NEW_POST) {
				refreshPostsWithSnackbar(R.string.success_new_post);
			}
		}
	}

	private void refreshPostsWithSnackbar(final int messageStringResourceId) {
		Snackbar.make(mCoordinatorLayout, messageStringResourceId, Snackbar.LENGTH_SHORT).show();
		mSwipeRefreshLayout.setRefreshing(true);
		refreshPosts();
	}
}
