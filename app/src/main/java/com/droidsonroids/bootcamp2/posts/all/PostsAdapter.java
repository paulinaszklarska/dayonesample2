package com.droidsonroids.bootcamp2.posts.all;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.droidsonroids.bootcamp2.R;
import com.droidsonroids.bootcamp2.model.Post;

import java.util.Collections;
import java.util.List;

class PostsAdapter extends BaseAdapter {

	private final LayoutInflater mLayoutInflater;
	private List<Post> mPosts = Collections.emptyList();

	public PostsAdapter(LayoutInflater layoutInflater) {
		mLayoutInflater = layoutInflater;
	}

	@Override public int getCount() {
		return mPosts.size();
	}

	@Override public Post getItem(final int position) {
		return mPosts.get(position);
	}

	@Override public long getItemId(final int position) {
		return position;
	}

	@Override public View getView(final int position, View convertView, final ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.list_view_item_post, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.mPostTitle = (TextView) convertView.findViewById(R.id.list_view_item_title);
			viewHolder.mPostBody = (TextView) convertView.findViewById(R.id.list_view_item_body);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		Post post = getItem(position);
		viewHolder.mPostTitle.setText(post.getTitle());
		viewHolder.mPostBody.setText(post.getBody());
		return convertView;
	}

	public void setPosts(final List<Post> posts) {
		mPosts = posts;
		notifyDataSetChanged();
	}

	public void deleteItem(final Post post) {
		mPosts.remove(post);
		notifyDataSetChanged();
	}

	static class ViewHolder {

		TextView mPostTitle;
		TextView mPostBody;
	}
}
